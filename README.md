This repository contains a LaTeX template for ANR 2019 JCJC grant
proposals, adapted from 

- a 2012 LaTeX template by Martin Monperrus:
  https://www.monperrus.net/martin/template-latex-anr
- the official LibreOffice template:
  http://www.agence-nationale-recherche.fr/fileadmin/aap/2019/aapg-anr-2019-pre-proposition.docx

Note: there also exists an English template

  http://www.agence-nationale-recherche.fr/fileadmin/aap/2019/aapg-anr-2019-pre-proposal.docx

but it is actually a bad translation of the French template, and in
particular its explanations of the expected content are less
informative. We included the English section titles in comment (yes,
"Partenariat" becomes "Partenariat"), and kept the French
explanations.
